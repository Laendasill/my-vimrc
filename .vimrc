set nocompatible
set t_Co=256
set nu
set expandtab
set timeoutlen=350
filetype off
colorscheme desert
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
	Plugin 'Shougo/neocomplete.vim.git'
	Plugin 'scrooloose/nerdtree.git'
	Plugin 'vim-airline/vim-airline.git'
"	Plugin 'craigemery/vim-autotag.git'
  Plugin 'justmao945/vim-clang'
"Plugin 'vim-scripts/vim-clang.git'
	Plugin 'tpope/vim-endwise.git'
	Plugin 'tpope/vim-haml.git'
	Plugin 'tpope/vim-rails.git'
	Plugin 'vim-ruby/vim-ruby.git'
	Plugin 'VundleVim/Vundle.vim.git'
	Plugin 'neovimhaskell/haskell-vim.git'
	Plugin 'tpope/vim-fugitive'
  Plugin 'pangloss/vim-javascript'
  Plugin 'mxw/vim-jsx'
  Plugin 'scrooloose/syntastic'
  Plugin 'Lokaltog/vim-distinguished'
  Plugin 'tpope/vim-surround'
"  Plugin 'szw/vim-tags'
  Plugin 'Xuyuanp/nerdtree-git-plugin'
 " Plugin 'wincent/command-t'
  Plugin 'ctrlpvim/ctrlp.vim'
"  Plugin 'Shutnik/jshint2.vim'
  Plugin 'rhysd/vim-grammarous'
  "Plugin 'vim-scripts/EasyGrep'
Plugin 'rking/ag.vim'
Plugin 'ntpeters/vim-better-whitespace'
call vundle#end()
"  
let g:ackprg = 'ag --nogroup --nocolor --column'
let g:NERDTreeIndicatorMapCustom = {
    \ "Modified"  : "✹",
    \ "Staged"    : "✚",
    \ "Untracked" : "✭",
    \ "Renamed"   : "➜",
    \ "Unmerged"  : "═",
    \ "Deleted"   : "✖",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ "Unknown"   : "?"
    \ }
filetype plugin indent on
let g:neocomplete#enable_at_startup = 1
let g:jsx_ext_required = 0
let g:syntastic_javascript_checkers = 'eslint'
let g:syntastic_javascript_eslint_exec = '/home/laendasillwork/.nvm/versions/node/v5.6.0/bin/eslint_d'
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><C-l>     neocomplete#complete_common_string()
autocmd FileType ruby set omnifunc=rubycomplete#Complete
au VIMEnter * NERDTree
set mouse=a

set tabstop=2
set shiftwidth=2
set expandtab
autocmd FileType c,cpp,java,scala let b:comment_leader = '// '
autocmd FileType sh,ruby,python   let b:comment_leader = '# '
autocmd FileType conf,fstab       let b:comment_leader = '# '
autocmd FileType tex              let b:comment_leader = '% '
autocmd FileType mail             let b:comment_leader = '> '
autocmd FileType vim              let b:comment_leader = '" '
noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>
vnoremap <C-r> "hy:%s/<C-r>h//gc<left><left><left>
nmap <F2> :SyntasticCheck eslint<CR>
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv
